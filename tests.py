#!/usr/bin/env python

import requests
import sys
import traceback


def test_get_index():
    r = requests.get(_api() + '/')
    assert(r.status_code == 200)
    assert(r.headers['content-type'] == 'application/json')
    body = r.json()
    assert(len(body.keys()) == 1)
    assert('Yoda' in body)
    assert(type(body['Yoda']) is unicode)
    assert(len(body['Yoda']) > 0)


def _api():
    return 'http://localhost:5000'


def _color(color_name, msg):
    return {
        'red':     '\x1b[31m',
        'green':   '\x1b[32m',
        'yellow':  '\x1b[33m',
        'blue':    '\x1b[34m'
    }[color_name] + msg + '\x1b[0m'


if __name__ == '__main__':
    if len(sys.argv) > 1:
        tests = sys.argv[1:]
    else:
        tests = [t for t in dir(sys.modules[__name__]) if t.startswith('test_')]
    for test in tests:
        try:
            globals().get(test)()
            print(_color('green', 'Passed ' + test))
        except Exception as e:
            print(_color('red', 'Failed ' + test + ' (' + str(e) + ')'))
            traceback.print_exc()
