from distutils.core import setup

setup(
    name='dkom',
    version='__VERSION__',
    description='d-kom example',
    url='https://github.concur.com/jirib/dkom',
    license='proprietary',
    scripts=['dkom'],
    data_files=[
        ('/etc', ['dkom.conf']),
        ('/usr/lib/systemd/system', ['dkom.service'])]
)
