VERSION=1.0

all: clean
	# building rpm
	mkdir build
	cp dkom dkom.conf dkom.service build
	sed "s/__VERSION__/$(VERSION)/" setup.py > build/setup.py
	echo 'systemctl daemon-reload' > build/after.script
	cd build && \
		fpm \
			--verbose \
			-s python \
			-t rpm \
			-n dkom \
			-v $(VERSION) \
			-d python-flask \
			--after-install after.script --after-remove after.script \
			--config-files /etc/dkom.conf \
			setup.py
	# publishing
	sudo cp build/*.rpm /var/localrepo
	sudo createrepo /var/localrepo
	# clean cache
	yum clean all

clean:
	rm -rf build
	find . -type f -name '*.pyc' -exec rm {} \;
