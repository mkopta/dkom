package { 'dkom':
  ensure => latest,
  notify => Service['dkom']
}

service { 'dkom':
  ensure    => running,
  enable    => true,
  subscribe => File['dkom.conf'],
}

file { 'dkom.conf':
  path    => '/etc/dkom.conf',
  ensure  => file,
  require => Package['dkom'],
  content => "Powerful you have become, the dark side I sense in you.
Always pass on what you have learned.
"
}
